<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSource extends Model
{
    protected $table = 'api_sources';
    
    protected $fillable = ['result_id','source_id','weather_description','temperature','pressure','humidity'];
    
}
