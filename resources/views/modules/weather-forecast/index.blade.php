@extends('layouts.master')

@section('content')
<div class="wrapper wrapper-content">
     <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Search Weather Forecast</h5>
                        <div class="ibox-tools">                              
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form id="search" role="form" action="{{ route('weather-forecast.store') }}" method="POST" class="form-horizontal">
                               @csrf
                                <div class="col-md-5"> 
                                    <div class="form-group">
                                    
                                        <input type="text" placeholder="Enter city" id="city" name ="city"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-5">    
                                    <div class="form-group">
                                        <label for="country" class="sr-only">Country</label>
                                        <input type="text" placeholder="Enter country" id="country" name="country"
                                            class="form-control ">
                                    </div>
                                </div>   
                                <div class="col-md-2">   
                                    <div class="form-group">
                                    <button class="btn btn-primary btn-block">Search</button>
                                    </div>
                                </div>   
                            </form>
                        </div>
                        <div class="loader" >
                            <div class="sk-spinner sk-spinner-rotating-plane loader" ></div>
                        </div>
                        <div class="row" id="targetlayer">
                            
                        </div>

                    </div>
                </div>
            </div>          
        </div>

        <div class="row">

            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Previous Weather Forecast Results </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        
                        </div>
                    </div>
                    <div class="ibox-content">
                        
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>                                    
                                    <th>City </th>
                                    <th>Country </th>
                                    <th>Source Api1 Temp </th>
                                    <th>Source Api2 Temp </th>
                                    <th>Average Temp</th>
                                    <th>Date Search</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $r) 
                                <tr>
                                    <td>{{ $r->city }}</td>
                                    <td>{{ $r->country }}</td>
                                    <td style="text-align:center">{{ ($r->getSources->where('source_id',1)->first())->temperature }}</td>
                                    <td style="text-align:center">{{ ($r->getSources->where('source_id',2)->first())->temperature}}</td>
                                    <td style="text-align:center">{{ $r->avg_temp}}</td>

                                    <td>{{ date('M d,Y h:i a',strtotime($r->created_at)) }}</td>                                  
                                </tr>
                                @endforeach
                                
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<div class="footer">
    <div class="pull-right">
        Ricky Millang - Php Developer
    </div>
    <div>
        <strong>Api Exam</strong> Link365 &copy; 2020
    </div>
</div>
@endsection

@section('custom_js')
<script>
         $(document).ready(function() { 

            toastr.options = {
                              "closeButton": true,
                              "debug": false,
                              "newestOnTop": false,
                              "progressBar": false,
                              "positionClass": "toast-top-right",
                              "preventDuplicates": false,
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "15000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                              };   

            $('.loader').hide();

         
            $('#search').submit(function(e) {	
                 var city = $("#city").val();
                 var country = $("#country").val();

                $('#targetlayer').html(""); 
                    
                    e.preventDefault();
                   
                    $(this).ajaxSubmit({ 
                        target:   '#targetLayer', 
                        dataType: 'text',
                        beforeSubmit: function() {
                            $('.loader').show();
                        },
                        uploadProgress: function (event, position, total, percentComplete){	
                           
                           $('.loader').show();
                        },
                        success:function (result){
                            toastr["success"]("Weather forecast result automatically save and listed in the previous forecast results table below. You can see the newly saved data if you refresh the page!");
                            $('.loader').hide();
                            $('#targetlayer').html(result);
                           
                        },
                        error:function(error){
                            toastr["error"](city+" city does not belong to "+country+" country!");
                            $('.loader').hide();
                            console.log(error);
                           
                        },
                        resetForm: true 
                    }); 
                    return false; 
                
            });
        });
   </script>
@endsection
