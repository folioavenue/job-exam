<!DOCTYPE html>
<html>

@include("includes.header")

<body class="top-navigation">

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="navbar-brand">Weather Forecast By Ricky Millang </div> 
                        </div>
                    </div>
                </div>
                
            </nav>
            </div>
          @yield("content")

        </div>
    </div>
    
   @include("includes.footer") 
    
   @yield("custom_js")
</body>

</html>

