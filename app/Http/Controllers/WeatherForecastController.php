<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Traits\ApiTrait;
use App\Result;

class WeatherForecastController extends Controller
{

    use ApiTrait; 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $api1_key, $api2_key;  

    public function __construct(){

        $this->api1_key = '439d4b804bc8187953eb36d2a8c26a02';
        //$api1_key - this api key is from OPEN WEATHER MAP API
        $this->api2_key = '0e79f2ef04eb0a7e1dc159eb8c71bda9';
        //$api2_key - this api key is from WEATHER STACK API

        $this->results = Result::orderBy('created_at','desc')->get();      
        
    }
    
    public function index()
    {
       // $data =  Http::get('openweathermap.org/data/2.5/weather?q='.$city.','.$country.'&appid=439d4b804bc8187953eb36d2a8c26a02')->json();
     
       return view('modules.weather-forecast.index',['results' => $this->results]);

       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = $request->city;
        $country = $request->country;
       
        $api1 =  $this->getApiData($city,$country,$this->api1_key,1);

        
        $api2 =  $this->getApiData($city,$country,$this->api2_key,2);
        
        if($api1['cod'] == 200 )
        {
            $temp_avg = $this->getAvgTemp($api1['main']['temp'],$api2['current']['temperature']);

            $this->storeData($city, $country, $temp_avg, $api1, $api2);
            
        }       

        return view('modules.weather-forecast.results',['api1' => $api1 ,'api2' => $api2,'city' => $city,'country' =>$country, 'temp_avg' => $temp_avg]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
