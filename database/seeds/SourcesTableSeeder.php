<?php

use Illuminate\Database\Seeder;
use App\Source;

class SourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Source::create(['api_name' => 'Open Weather Map Api']);
        Source::create(['api_name' => 'Weather Stack Api']);
    }
}
