<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = ['city','country','avg_temp'];

    public function getSources()
    {
        return $this->hasMany('App\ApiSource','result_id','id');
    }
    
}
