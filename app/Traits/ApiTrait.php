<?php

namespace App\Traits;
use Illuminate\Support\Facades\Http;
use App\Result;
use App\ApiSource;

trait ApiTrait
{
    /* Parameters
    * $city - user city input 
    * $country - user country input
    * $api - api key 
    * $source - which api source value 1 or 2
    * api source value 1 - Open Weather Map Api
    * api source value 2 - Weather Stack Api
    */

    public function getApiData($city, $country, $api, $source)
    {
        if($source == 1){

            $data = Http::get('openweathermap.org/data/2.5/weather?q='.$city.','.$country.'&appid='.$api)->json();
        
        }else{

            $data = Http::get('api.weatherstack.com/current?access_key='.$api.'&query='.$city.','.$country)->json();

        }

        return $data;
    }

    public function getAvgTemp($temp1, $temp2)
    {
        $avg = ($temp1 + $temp2) / 2;
        
        return $avg;
    }

    public function storeData($city, $country, $avg_temp, $api1, $api2)
    {
        

        $result = Result::create([
                                'city' => $city, 
                                 'country' => $country,
                                 'avg_temp' => $avg_temp,
                                ]);

        if($result){
                //store data of api1
                ApiSource::create([
                                'result_id' => $result->id,
                                'source_id' => 1,
                                'weather_description' => $api1['weather'][0]['description'],
                                'temperature' => $api1['main']['temp'],
                                'pressure' => $api1['main']['pressure'],
                                'humidity' => $api1['main']['humidity'],
                                ]);

                 //store data of api2
                 ApiSource::create([
                    'result_id' => $result->id,
                    'source_id' => 2,
                    'weather_description' => $api2['current']['weather_descriptions'][0],
                    'temperature' => $api2['current']['temperature'],
                    'pressure' => $api2['current']['pressure'],
                    'humidity' => $api2['current']['humidity'],
                    ]);                 

        } 
        
        return $result;

    }
    
}