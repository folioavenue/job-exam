<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Ricky Api Test - Weather Forecast</title>

    <link href="{{ asset('inspinia_admin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('inspinia_admin/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('inspinia_admin/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    
    <!-- Ladda style -->
    <link href="{{ asset('inspinia_admin/css/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('inspinia_admin/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet"> 
    <!-- iCheck style -->
    
    <link href="{{ asset('inspinia_admin/css/style.css') }}" rel="stylesheet">



    <!-- Sweet Alert -->
    <link href="{{ asset('inspinia_admin/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">   

</head>