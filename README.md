<p>
This repository is a simple Weather forecast web application that generate current weather in any country and city. I used two free different api sources for weather forecast result which is listed below. 
</p>

## Api sources
**Open Weather Map Api** - The service api allows you to regularly download current weather and forecast data in JSON format. It is a free used api.

**Weather Stack Api** - Real-Time & Historical World Weather Data API that retrieve instant, accurate weather information for any location in the world in lightweight JSON format.

## Api Key & Sample URL Api Request

**Open Weather Map Api**

Api_key : 439d4b804bc8187953eb36d2a8c26a02

Sample Url : http://openweathermap.org/data/2.5/weather?q=&appid=439d4b804bc8187953eb36d2a8c26a02'


**Weather Stack Api**

Api_key : 0e79f2ef04eb0a7e1dc159eb8c71bda9

Sample Url : http://api.weatherstack.com/current?access_key=0e79f2ef04eb0a7e1dc159eb8c71bda9&query=London,UK