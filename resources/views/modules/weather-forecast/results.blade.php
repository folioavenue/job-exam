<div class="col-md-6">
    <h2>Source Api1</h2>
    <small><strong> Open Weather Map Api </strong></small>
    <ul class="list-group clear-list m-t">
        <li class="list-group-item fist-item">
            <span class="pull-right">
                {{ ucfirst($city) }}
            </span>
            <span class="label label-success "><i class="fa fa-map-marker"></i></span> City
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ ucfirst($country) }}
            </span>
            <span class="label label-info"><i class="fa fa-flag"></i></span> Country
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ ucfirst($api1['weather'][0]['description']) }}
            </span>
            <span class="label label-primary"><i class="fa fa-cloud"></i></span> Weather Description
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ $api1['main']['temp'] }}
            </span>
            <span class="label label-danger"><i class="fa fa-sun-o"></i></span> Temperature
        </li>        
        <li class="list-group-item">
            <span class="pull-right">
                {{ $api1['main']['pressure'] }}
            </span>
            <span class="label label-success"><i class="fa fa-dashboard"></i></span> Pressure
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ $api1['main']['humidity'] }}
            </span>
            <span class="label label-danger"><i class="fa fa-tint"></i></span> Humidity
        </li>
    </ul>
</div>
<div class="col-md-6">
    <h2>Source Api2</h2>
    <small><strong> Weather Stack Api </strong></small>
    <ul class="list-group clear-list m-t">
    <li class="list-group-item fist-item">
            <span class="pull-right">
                {{ ucfirst($city) }}
            </span>
            <span class="label label-success "><i class="fa fa-map-marker"></i></span> City
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ ucfirst($country) }}
            </span>
            <span class="label label-info"><i class="fa fa-flag"></i></span> Country
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ ucfirst($api2['current']['weather_descriptions'][0]) }}
            </span>
            <span class="label label-primary"><i class="fa fa-cloud"></i></span> Weather Description
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ $api2['current']['temperature'] }}
            </span>
            <span class="label label-danger"><i class="fa fa-sun-o"></i></span> Temperature
        </li>

        <li class="list-group-item">
            <span class="pull-right">
                {{ $api2['current']['pressure'] }}
            </span>
            <span class="label label-success"><i class="fa fa-dashboard"></i></span> Pressure
        </li>
        <li class="list-group-item">
            <span class="pull-right">
                {{ $api2['current']['humidity'] }}
            </span>
            <span class="label label-danger"><i class="fa fa-tint"></i></span> Humidity
        </li>
    </ul>
    
</div>
<button class="btn btn-primary btn-block">Average Temperature : <b>{{ $temp_avg }}</b></button>