<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_sources', function (Blueprint $table) {
            $table->id();
            $table->foreignId('result_id')->unsigned();
            $table->foreignId('source_id')->unsigned();
            $table->string('weather_description');
            $table->decimal('temperature',9,2);
            $table->integer('pressure');
            $table->integer('humidity');
            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('results');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_sources');
    }
}
